import java.util.Arrays;

public class MinMax{
    public static void main(String args[]){
        int[] numbers = new int[5];
        
        for(int index = 0; index < 5; index++)
        {
            numbers[index] = Integer.parseInt(args[index]);
        }
        Arrays.sort(numbers);
        System.out.println("Maximum number is : " + numbers[4]);
        System.out.println("Minimum number is:  " + numbers[0]);
    }
}