public class PayTax{
    public static void main(String args[]){
        String valString = args[0];
        double valDecimal = Float.valueOf(valString)*0.15;
        System.out.println("Tax on the bill amount is : " + valDecimal);
    }
}