public class CurrentAccount {

  private static long accountNumberTracker = 10000;

  private long accountNumber;

  private double accountBalance;

  private String customerName;

  private Address address;

  private String businessName;

  private String gstNumber;

  public long getAccountNumber() {
      return accountNumber;
  }

  public CurrentAccount(String customerName, String gstNumber, String businessName) {
      this.customerName = customerName;
      this.accountBalance = 50000;
      this.accountNumber = ++ accountNumberTracker;
      this.businessName = businessName;
      this.gstNumber = gstNumber;
  }

  public CurrentAccount(String customerName, double initialAccountBalance, String gstNumber, String businessName) {
      this.customerName = customerName;
      this.accountBalance = initialAccountBalance;
      this.accountNumber = ++ accountNumberTracker;
      this.gstNumber = gstNumber;
  }

  public CurrentAccount(String customerName, double initialAccountBalance, Address add, String gstNumber) {
      this.customerName = customerName;
      this.accountBalance = initialAccountBalance;
      this.address = add;
      this.accountNumber = ++ accountNumberTracker;
      this.gstNumber = gstNumber;
  }

  public CurrentAccount(String customerName, Address add, String gstNumber) {
      this.customerName = customerName;
      this.accountBalance = 50000;
      this.address = add;
      this.accountNumber = ++ accountNumberTracker;
      this.gstNumber = gstNumber;
  }

  public double withdraw(double amount) {
      if ((this.accountBalance - 50000) >= amount) {
          this.accountBalance = this.accountBalance - amount;
          return amount;
      }
      return 0;
  }

  public double checkBalance() {
      return this.accountBalance;
  }

  public double deposit(double amount) {
      this.accountBalance = accountBalance + amount;
      return accountBalance;
  }

  public String getCustomerName() {
      return this.customerName;
  }
}