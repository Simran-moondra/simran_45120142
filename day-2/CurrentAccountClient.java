public class CurrentAccountClient {

  public static void main(String[] args) {

      CurrentAccount rajeesh = new CurrentAccount("Rajeesh", "gjHDJAD72893", "HP");
      System.out.println("Customer Name: " + rajeesh.getCustomerName());
      System.out.println("Account Number " + rajeesh.getAccountNumber());
      System.out.println("Initial Account balance " + rajeesh.checkBalance());
      System.out.println("Account Balance " + rajeesh.deposit(5000));
      System.out.println("Balance after deposit" + rajeesh.checkBalance());
      rajeesh.withdraw(2000);
      System.out.println("Balance after Withdraw " + rajeesh.checkBalance());

      System.out.println(" ---------------------------------");

      CurrentAccount naveen = new CurrentAccount("Naveen", 80000, "mdKHHSF8484", "abc");
      System.out.println("Account Number " + naveen.getAccountNumber());
      System.out.println("Customer Name: " + naveen.getCustomerName());
      System.out.println("Initial Account balance " + naveen.checkBalance());
      naveen.withdraw(40000);
      System.out.println("Balance after Withdraw " + naveen.checkBalance());

      System.out.println(" ---------------------------------");

      Address add = new Address("8th Ave", "Bangalore", "Karnataka", 577142);
      CurrentAccount vinay = new CurrentAccount("Vinay", add, "TWYHSF8484");
      System.out.println("Customer Name: " + vinay.getCustomerName());
  }
}