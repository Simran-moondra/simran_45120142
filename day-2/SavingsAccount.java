public class SavingsAccount {

    
    private static long accountNumberTracker = 1000;

   
    private long accountNumber;

    private double accountBalance;

    private String customerName;

    private Address address;

    private String emailAddress;

    private String nominee;

    private String nationality;

    public long getAccountNumber() {
        return accountNumber;
    }

    public SavingsAccount(String customerName, double initialAccountBalance) {
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;
        this.nationality = "INDIAN";
        this.accountNumber = ++accountNumberTracker;
    }

    public SavingsAccount(String customerName, double initialAccountBalance, String nominee) {
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;
        this.nominee = nominee;
        this.nationality = "INDIAN";
        this.accountNumber = ++accountNumberTracker;
    }

    public SavingsAccount(String customerName, double initialAccountBalance, Address address) {
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;
        this.address = address;
        this.nationality = "INDIAN";
        this.accountNumber = ++accountNumberTracker;
    }

    public SavingsAccount(String customerName, double initialAccountBalance, String nominee, String nationality) {
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;
        this.nominee = nominee;
        this.nationality = nationality;
        this.accountNumber = ++accountNumberTracker;
    }

    // instance methods
    public double withdraw(double amount) {
        if (this.accountBalance >= amount) {
            this.accountBalance = this.accountBalance - amount;
            return amount;
        }
        return 0;
    }

    public double checkBalance() {
        return this.accountBalance;
    }

    public void transferAmount(double amount, SavingsAccount user){
        if(this.accountBalance >= amount){
            this.accountBalance = this.accountBalance - amount;
            user.accountBalance = user.accountBalance + amount;
        }
        else {
            System.out.println("Sorry! you do not have the sufficient account balance!!")
        }
    }

    public double deposit(double amount) {
        this.accountBalance = accountBalance + amount;
        return accountBalance;
    }

    public double deposit(double amount, SavingsAccount user) {
        user.accountBalance = accountBalance + amount;
        return accountBalance;
    }

    public double deposit(double amount, String notes) {
        this.accountBalance = accountBalance + amount;
        System.out.println("notes: " + notes);
        return accountBalance;
    }

    public void updateAddress(Address address) {
        this.address = address;
    }

    public String getCustomerName() {
        return this.customerName;

    }

    public static long getCurrentCounterValue() {
        return accountNumberTracker;
    }
}
