public class callbyref {

    public static void main(String[] args) {

        int array[] = new int[] { 50, 60, 70, 80 };

        System.out.println("Changes before the call");
        for (int s : array) {
            System.out.println(s);
        }
        System.out.println("-------------------------------");
        callByRef(array);

        System.out.println("Changes before the call");
        for (int s : array) {
            System.out.println(s);
        }
    }

    

    private static void callByRef(int[] array2) {
        System.out.println("-------------------------------");
        System.out.print("Changes inside the method");

        array2[0] = 10;
        array2[1] = 20;
        array2[2] = 30;
        array2[3] = 40;
        for (int s : array2) {
            System.out.println(s);
        }

    }
}
