abstract class BankAccount{
    private static long accountNumberTracker = 1000;

   
    public long accountNumber;

    public double accountBalance;

    public double loanamount;

    public String customerName;

    public Address address;

    public String emailAddress;

    public BankAccount(String customerName, double initialAccountBalance) {
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;
        this.accountNumber = ++accountNumberTracker;
    }

    public abstract double withdraw(double amount);

    public final double deposit(double amount){
        this.accountBalance = accountBalance + amount;
        return accountBalance;
    }

    public final double checkBalance() {
        return this.accountBalance;
    }

    public abstract double loan(double amount);


}

final class SavingsAccount extends BankAccount{

    public SavingsAccount(String customerName, double initialAccountBalance) {
        super(customerName, initialAccountBalance);
    }

    public double withdraw(double amount){
        Savingswithdraw();
    }

    public double deposit(double amount);

    public double checkBalance();

    public double loan(double amount){
        Savingsloan();
    }
    public double Savingswithdraw(double amount){
        if ((this.accountBalance -10000) >= amount) {
            this.accountBalance = this.accountBalance - amount;
            return amount;
        }
        return 0;
    }

    
    public double Savingsloan(double amount){
        if(amount <= 500000){
            System.out.println("You are eligible");
        }
        else {
            System.out.println("You are not eligible!");
        }
    }
}

final class CurrentAccount extends BankAccount{

    private String businessname;
    private String gstnum;

    public CurrentAccount(String customerName, double initialAccountBalance, String gst, String bussiness) {
        super(customerName, initialAccountBalance);
        this.gstnum = gst;
        this.businessname = bussiness;
    }

    public double withdraw(double amount){
        Currentwithdraw();
    }

    public double deposit(double amount);

    public double checkBalance();

    public double loan(double amount){
        Currentloan();
    }
    public double Currentwithdraw(double amount){
        if ((this.accountBalance -25000) >= amount) {
            this.accountBalance = this.accountBalance - amount;
            return amount;
        }
        return 0;
    }

    
    public double Currentloan(double amount){
        if(amount <= 2500000){
            System.out.println("You are eligible");
        }
        else {
            System.out.println("You are not eligible!");
        }
    }
}

final class SalariedAccount extends BankAccount{

    public SalariedAccount(String customerName, double initialAccountBalance) {
        super(customerName, initialAccountBalance);
    }

    public double withdraw(double amount){
        Salariedwithdraw();
    }

    public double deposit(double amount);

    //public double checkBalance();

    public double loan(double amount){
        Salariedloan();
    }
    public double Salariedwithdraw(double amount){
        if ((this.accountBalance -15000) >= amount) {
            this.accountBalance = this.accountBalance - amount;
            return amount;
        }
        return 0;
    }

    
    public double Salariedloan(double amount){
        if(this.amount <= 1000000){
            System.out.println("You are eligible");
        }
        else {
            System.out.println("You are not eligible!");
        }
    }
}

public class BankAccountcheck{
    public static void main(string[] args){

        BankAccount ba = null;

        String acctype = args[0];

        BankAccount simran = new BankAccount("Simran",375832929,10000000);

        if(acctype.equals("1")){
            simran = new SavingsAccount();
        }
        else if(acctype.equals("2")){
            simran = new CurrentAccount();
        }
        else if(acctype.equals("3")){
            simran = new SalariedAccount();
        }
    }

    
    
}

