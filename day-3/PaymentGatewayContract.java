interface Paymentgateway {
    void payment(String fromwho, String towhom, double amount, String msg);
}

interface Mobilerecharge {
    void recharge(String mobilenumber, double amount);
}

class Paytm implements Paymentgateway, Mobilerecharge {
    public void payment(String fromwho, String towhom, double amount, String msg){
        System.out.println("Payment from: " + fromwho + "Payment to : " + towhom + "the amount is : " + amount + "message : Using Paytm -" + msg);
    }

    public void recharge(String mobilenumber, double amount) {
        System.out.println("Mobile recharge for " + mobilenumber + " with amount  " + amount + " with Paytm");
    }

    
}

class AmazonPay implements Paymentgateway, Mobilerecharge {
    public void payment(String fromwho, String towhom, double amount, String msg){
        System.out.println("Payment from: " + fromwho + "Payment to : " + towhom + "the amount is : " + amount + "message : Using Amazonpay -" + msg);
    }

    public void recharge(String mobilenumber, double amount) {
        System.out.println("Mobile recharge for " + mobilenumber + " with amount  " + amount + " with AmazonPay");
    }
}

class Phonepe implements Paymentgateway {
    public void payment(String fromwho, String towhom, double amount, String msg){
        System.out.println("Payment from: " + fromwho + "Payment to : " + towhom + "the amount is : " + amount + "message : Using Paytm -" + msg);
    }

}

public class PaymentGatewayContract {
    public static void main(String[] args) {

        Paymentgateway pG = null;
        Mobilerecharge mR = null;
        if (args[0] == "1") {
            Paytm pt = new Paytm();
            pG = pt;
            mR = pt;
        } else if (args[0].equals("2")){
            AmazonPay ap = new AmazonPay();
            pG = ap;
            mR = ap;
        }
        else if (args[0].equals("3"))
        {
            Phonepe pp = new Phonepe();
            pG = pp;
        }
        pG.payment("Simran", "Nikita", 200, "Please confirm");
        
        if(mR != null){
        mR.recharge("123456789", 550);
        }
    }
}




