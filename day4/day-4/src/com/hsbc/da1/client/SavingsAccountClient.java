package com.hsbc.da1.client;

import com.hsbc.da1.controller.SavingsAccountController;
import com.hsbc.da1.model.*;

public class SavingsAccountClient {

	public static void main(String[] args) {
		
		SavingsAccountController accountController = new SavingsAccountController();
		
		Address addr = new Address("Pune", 411015);
		SavingsAccount savingsAccount = accountController.openSavingsAccount("Simran", 100000, addr);
		
		SavingsAccount tosavingsAccount = accountController.openSavingsAccount("Mahak", 100000);
		
		System.out.println("Account Id : " + savingsAccount.getAccountNumber());
		System.out.println("Account Id : " + tosavingsAccount.getAccountNumber());
		
		System.out.println("AccountBalance for : " + savingsAccount.getAccountNumber() + " after deposit is : " + accountController.deposit(10001, 5000));
		
		SavingsAccount[] savingsAccounts = accountController.fetchSavingsAccount();
		for(SavingsAccount SavingsAccount : savingsAccounts) {
			if(savingsAccount != null) {
				System.out.println("Account Id : " + savingsAccount.getAccountNumber());
				System.out.println("Customer Name : " + savingsAccount.getAccountNumber());
			}
		}

	}

}
