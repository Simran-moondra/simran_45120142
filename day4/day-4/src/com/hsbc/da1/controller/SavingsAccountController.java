package com.hsbc.da1.controller;
import com.hsbc.da1.service.*;
import com.hsbc.da1.model.*;

public class SavingsAccountController {
	
	private SavingsAccountService savingsAccountService = new SavingsAccountService();
	
	public SavingsAccount openSavingsAccount(String customerName, double amount) {
		SavingsAccount savingsAccount = this.savingsAccountService.createSavingsAccount(customerName, amount);
		return savingsAccount;
	}
	
	public SavingsAccount openSavingsAccount(String customerName, double amount, Address address) {
		SavingsAccount savingsAccount = this.savingsAccountService.createSavingsAccount(customerName, amount, address);
		return savingsAccount;
	}
	
	public void deleteSavingsAccount(long accountId) {
		this.savingsAccountService.deleteSavingsAccount(accountId);
	}
	
	public SavingsAccount[] fetchSavingsAccount() {
		SavingsAccount[] savingsAccount = this.savingsAccountService.fetchSavingsAccount();
		return savingsAccount;
	}
	
	public SavingsAccount fetchSavingsAccountById(long accountId) {
		SavingsAccount savingsAccount = this.savingsAccountService.fetchSavingsAccountByAccountId(accountId);
		return savingsAccount;
	}
	
	public double withdraw(long accountId, double amount) {
		double updatedAmount = this.savingsAccountService.withdraw(accountId, amount);
		return updatedAmount;
	}
	
	public double deposit(long accountId, double amount) {
		return this.savingsAccountService.deposit(accountId, amount);
	}
	
	public double checkBalance(long accountId) {
		return this.savingsAccountService.checkBalance(accountId);
	}
}
