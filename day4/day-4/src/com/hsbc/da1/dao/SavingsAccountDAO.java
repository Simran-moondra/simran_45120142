package com.hsbc.da1.dao;
import com.hsbc.da1.model.*;
import com.hsbc.da1.service.*;

public class SavingsAccountDAO {
	private static SavingsAccount[] savingsAccounts= new SavingsAccount[50];
	private static int counter = 0;
	
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
		this.savingsAccounts[counter++] = savingsAccount;
		return savingsAccount;
	}
	
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
		for(int index = 0; index < savingsAccounts.length; index++) {
			if(this.savingsAccounts[index].getAccountNumber() == accountNumber) {
				this.savingsAccounts[index] = savingsAccount;
				break;
			}
		}
		return savingsAccount;
	}
	
	public void deleteSavingsAccount(long accountNumber) {
		for(int index = 0; index < savingsAccounts.length; index++) {
			if(this.savingsAccounts[index].getAccountNumber() == accountNumber) {
				this.savingsAccounts[index] = null;
				break;
			}
		}
	}
	
	public SavingsAccount[] fetSavingsAccount() {
		return savingsAccounts;
	}
	
	public SavingsAccount fetchSavingsAccountById(long accountNumber) {
		for(int index = 0; index < savingsAccounts.length; index++) {
			if(this.savingsAccounts[index].getAccountNumber() == accountNumber) {
				return this.savingsAccounts[index];
			}
		}
		return null;
	}
		
}
	

