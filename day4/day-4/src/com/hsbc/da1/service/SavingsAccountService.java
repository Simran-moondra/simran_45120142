package com.hsbc.da1.service;
import com.hsbc.da1.model.*;
import com.hsbc.da1.dao.*;


public class SavingsAccountService {
	
	private SavingsAccountDAO dao = new SavingsAccountDAO();
	
	public SavingsAccount createSavingsAccount(String customerName, double accountBalance) {
		SavingsAccount savingsAccount = new SavingsAccount(customerName, accountBalance);
		SavingsAccount newAcc = dao.saveSavingsAccount(savingsAccount);
		return newAcc;
	}
	
	public SavingsAccount createSavingsAccount(String customerName, double accountBalance, Address address) {
		SavingsAccount savingsAccount = new SavingsAccount(customerName, accountBalance, address);
		return this.dao.saveSavingsAccount(savingsAccount);
	}
	
	public void deleteSavingsAccount(long accountNumber) {
		this.dao.deleteSavingsAccount(accountNumber);
	}
	
	public SavingsAccount[] fetchSavingsAccount() {
		SavingsAccount[] savingsAccounts = this.dao.fetSavingsAccount();
		return savingsAccounts;
	}
	
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountById(accountNumber);
		return savingsAccount;
	}
	
	public double withdraw(long accountId, double amount) {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountById(accountId);
		if(savingsAccount.getAccountBalance() > (amount + 1000)) {
			savingsAccount.setAccountBalance(savingsAccount.getAccountBalance() - amount);
			this.dao.updateSavingsAccount(accountId, savingsAccount);
			return amount;
		}
		return 0;
	}
	
	public double deposit(long accountId, double amount) {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountById(accountId);
		if(savingsAccount != null) {
			savingsAccount.setAccountBalance(savingsAccount.getAccountBalance() + amount);
			this.dao.updateSavingsAccount(accountId, savingsAccount);
			return savingsAccount.getAccountBalance();
		}
		return 0;
	}
	
	public double checkBalance(long accountId) {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountById(accountId);
		return savingsAccount.getAccountBalance();
	}
		
}
