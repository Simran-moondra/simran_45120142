package com.hsbc.da1.client;
import com.hsbc.da1.model.*;
import com.hsbc.da1.controller.*;

public class ItemClient {
	
	public static void main(String args[]) {
	ItemController controller = new ItemController();
	
	Item shirt = controller.readItem("Pringles Chips", 2, 200.50);
	
	Item pants = controller.readItem("Cadbury Silk", 1, 180.00);
	
	Item purse = controller.readItem("Olive Oil", 1, 2000.00);
	
	Item[] items = controller.fetchallItems();

	for (Item item : items) {
		if (item != null) {
			System.out.println("***************************************************");
			System.out.println("Item Name : " + item.getItemname());
			System.out.println("Price of the Item : " + item.getItemprice());
			System.out.println("Item Quantity : " + item.getItemquantity());
			System.out.println("***************************************************");
		} else {
			break;
		}
	}

}
}
