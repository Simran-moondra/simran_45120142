package com.hsbc.da1.controller;
import com.hsbc.da1.model.*;
import com.hsbc.da1.service.*;


public class ItemController {
	ItemService service = new ItemServiceImplement();
	
    public Item readItem(String Itemname, int Itemquantity, double Itemprice) {
    	
    	return this.service.createItem(Itemname, Itemquantity, Itemprice);
    }
	
	public void removeItem(long Itemid) {
		
		this.service.removeItem(Itemid);
	}
	
	public Item[] fetchallItems() {
		
		return this.service.fetchallItems();
	}
	
	public Item fetchItembyId(long Itemid) {
		
		return this.service.fetchItembyId(Itemid);
	}
}
