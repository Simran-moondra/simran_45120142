package com.hsbc.da1.dao;

import com.hsbc.da1.model.*;

public interface ItemDAO {
	
	Item saveItem(Item item);
	
	void removeItem(long Itemid);
	
	Item updateItem(long Itemid, Item item);
	
	Item[] fetchItems();
	
	Item fetchItembyId(long Itemid);
}
