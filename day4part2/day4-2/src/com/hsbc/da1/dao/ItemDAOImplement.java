package com.hsbc.da1.dao;

import com.hsbc.da1.model.*;

public class ItemDAOImplement implements ItemDAO {
	
	private static Item[] items = new Item[100];
	private static int counter;


	@Override
	public Item saveItem(Item item) {
		// TODO Auto-generated method stub
		items[counter++] = item;
		return item;
	}

	@Override
	public void removeItem(long Itemid) {
		// TODO Auto-generated method stub
		for(int index = 0; index<items.length; index++) {
			if(items[index].getItemid() == Itemid) {
				items[index] = null;
				break;
			}
		}
	}
	
	

	@Override
	public Item updateItem(long Itemid, Item item) {
		// TODO Auto-generated method stub
		for(int index = 0; index<items.length; index++) {
			if(items[index].getItemid() == Itemid) {
				items[index] = item;
				return item;
			}
		}
		return null;
	}

	@Override
	public Item[] fetchItems() {
		// TODO Auto-generated method stub
		return items;
	}

	@Override
	public Item fetchItembyId(long Itemid) {
		// TODO Auto-generated method stub
		for(int index = 0; index<items.length; index++) {
			if(items[index].getItemid() == Itemid) {
				return items[index];
			}
		}
		return null;
	}

	
}
