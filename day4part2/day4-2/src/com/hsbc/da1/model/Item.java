package com.hsbc.da1.model;

public class Item {

    private long Itemid;
	
	private String Itemname;
	
	private double Itemprice;
	
	private int Itemquantity;
	
	private static long Itemidcounter = 100;
	
	public Item(String Itemname, int Itemquantity,  double Itemprice) {
		this.Itemname = Itemname;
		this.Itemquantity = Itemquantity;
		this.Itemprice = Itemprice;
		this.Itemid = ++Itemidcounter;
	}

	public String getItemname() {
		return Itemname;
	}

	public void setItemname(String itemname) {
		this.Itemname = itemname;
	}

	
	
	public double getItemprice() {
		return Itemprice;
	}

	public void setItemprice(double itemprice) {
		this.Itemprice = itemprice;
	}

	
	
	public int getItemquantity() {
		return Itemquantity;
	}

	public void setItemquantity(int itemquantity) {
		this.Itemquantity = itemquantity;
	}
	
	

	public long getItemid() {
		return Itemid;
	}
	
	

}
