package com.hsbc.da1.service;
import com.hsbc.da1.model.*;

public interface ItemService {

	Item createItem(String Itemname, int Itemquantity, double Itemprice);
	
	void removeItem(long Itemid);
	
	Item[] fetchallItems();
	
	Item fetchItembyId(long Itemid);
	
}
