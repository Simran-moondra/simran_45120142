package com.hsbc.da1.service;
import com.hsbc.da1.model.*;
import com.hsbc.da1.dao.*;

public class ItemServiceImplement implements ItemService {
	
	ItemDAO dao = new ItemDAOImplement();

	@Override
	public Item createItem(String Itemname, int Itemquantity, double Itemprice) {
		// TODO Auto-generated method stub
		Item item = new Item(Itemname, Itemquantity,Itemprice);
		Item itemcreated = this.dao.saveItem(item);
		return itemcreated;
	}

	@Override
	public void removeItem(long Itemid) {
		// TODO Auto-generated method stub
		this.dao.removeItem(Itemid);
		
	}

	@Override
	public Item[] fetchallItems() {
		// TODO Auto-generated method stub
		return this.dao.fetchItems();
	}

	@Override
	public Item fetchItembyId(long Itemid) {
		// TODO Auto-generated method stub
		return this.dao.fetchItembyId(Itemid);
	}

}
