package com.hsbc.da1.client;

import java.util.List;

import com.hsbc.da1.controller.*;
import com.hsbc.da1.exception.*;
import com.hsbc.da1.model.*;

public class SavingsAccountClient {

	public static void main(String[] args) throws InsufficientBalanceException{
		
		SavingsAccountController accountController = new SavingsAccountController();
		
		
		SavingsAccount simranSavingsAccount = accountController.openSavingsAccount("Simran", 100000);
		
		SavingsAccount mahakSavingsAccount = accountController.openSavingsAccount("Mahak", 100000);
		
		System.out.println("Account Number : " + simranSavingsAccount.getAccountNumber());
		System.out.println("Customer Name : " + simranSavingsAccount.getCustomerName());
		System.out.println("Initial Account Balance after Account Opening : " + simranSavingsAccount.getAccountBalance());
		System.out.println("---------------------------------------------------------------------------------");
		System.out.println("Account Number : " + mahakSavingsAccount.getAccountNumber());
		System.out.println("Customer Name : " + mahakSavingsAccount.getCustomerName());
		System.out.println("Initial Account Balance after Account Opening : " + mahakSavingsAccount.getAccountBalance());
		
		try {
			System.out.println("---------------------------------------------------------------------------------");
			System.out.println("AccountBalance for : " + simranSavingsAccount.getAccountNumber() + " after deposit is : " + accountController.deposit(10001, 5000));
		}catch(CustomerNotFound e) {
			System.out.println(e.getMessage());
		}
		System.out.println("---------------------------------------------------------------------------------");
		List<SavingsAccount> savingsAccounts = accountController.fetchSavingsAccount();
		for(SavingsAccount savingsAccount : savingsAccounts) {
			if(savingsAccount != null) {
				System.out.println("Account Number : " + savingsAccount.getAccountNumber());
				System.out.println("Customer Name : " + savingsAccount.getCustomerName());
				System.out.println("---------------------------------------------------------------------------------");
			}
		}
		
		System.out.println("---------------------------------------------------------------------------------");
		try {
			System.out.println("Withdraw : " + accountController.withdraw(10001, 1000000));
		}catch(InsufficientBalanceException | CustomerNotFound e) {
			System.out.println(e.getMessage());
		}
		
		System.out.println("---------------------------------------------------------------------------------");
		
		try {
			System.out.println("User Account Details : " + accountController.fetchSavingsAccountById(10001).getCustomerName());
			System.out.println("User Account Details : " + accountController.fetchSavingsAccountById(10011).getCustomerName());
		}catch(CustomerNotFound e) {
			System.out.println(e.getMessage());
		}
		
		System.out.println("---------------------------------------------------------------------------------");
	}

}
