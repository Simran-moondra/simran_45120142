package com.hsbc.da1.controller;
import java.util.List;

import com.hsbc.da1.exception.*;
import com.hsbc.da1.model.*;
import com.hsbc.da1.service.*;


public class SavingsAccountController {
	
	
private SavingsAccountService savingsAccountService = new SavingsAccountServiceImpl();
	
	public SavingsAccount openSavingsAccount(String customerName, double amount) {
		SavingsAccount savingsAccount = this.savingsAccountService.createSavingsAccount(customerName, amount);
		return savingsAccount;
	}
	
	public void deleteSavingsAccount(long accountNumber) {
		this.savingsAccountService.deleteSavingsAccount(accountNumber);
	}
	
	public List<SavingsAccount> fetchSavingsAccount() {
		List<SavingsAccount> savingsAccount = this.savingsAccountService.fetchSavingsAccount();
		return savingsAccount;
	}
	
	public SavingsAccount fetchSavingsAccountById(long accountNumber) throws CustomerNotFound{
		SavingsAccount savingsAccount = this.savingsAccountService.fetchSavingsAccountByAccountId(accountNumber);
		return savingsAccount;
	}
	
	public double withdraw(long accountNumber, double amount) throws InsufficientBalanceException, CustomerNotFound{
		double updatedAmount = this.savingsAccountService.withdraw(accountNumber, amount);
		return updatedAmount;
	}
	
	public double deposit(long accountNumber, double amount) throws CustomerNotFound {
		return this.savingsAccountService.deposit(accountNumber, amount);
	}
	
	public double checkBalance(long accountNumber) throws CustomerNotFound {
		return this.savingsAccountService.checkBalance(accountNumber);
	}

}
