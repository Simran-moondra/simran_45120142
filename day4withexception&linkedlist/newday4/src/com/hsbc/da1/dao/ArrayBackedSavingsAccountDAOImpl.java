package com.hsbc.da1.dao;
import java.util.*;
import com.hsbc.da1.model.*;
import com.hsbc.da1.exception.*;

public class ArrayBackedSavingsAccountDAOImpl implements SavingsAccountDAO{
	
	private static SavingsAccount[] savingsAccounts = new SavingsAccount[50];
	private static int counter = 0;
	

	@Override
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		this.savingsAccounts[counter++] = savingsAccount;
		return savingsAccount;
	}

	@Override
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		for(int index = 0; index < savingsAccounts.length; index++) {
			if(this.savingsAccounts[index].getAccountNumber() == accountNumber) {
				this.savingsAccounts[index] = savingsAccount;
				break;
			}
		}
		return savingsAccount;

		
	}

	@Override
	public void deleteSavingsAccount(long accountNumber) {
		// TODO Auto-generated method stub
		for(int index = 0; index < savingsAccounts.length; index++) {
			if(this.savingsAccounts[index].getAccountNumber() == accountNumber) {
				this.savingsAccounts[index] = null;
				break;
			}
		}
		
	}

	@Override
	public List<SavingsAccount> fetchSavingsAccount() {
		// TODO Auto-generated method stub
		return Arrays.asList(savingsAccounts);
	}

	@Override
	public SavingsAccount fetchByIdSavingsAccount(long accountNumber) throws CustomerNotFound {
		// TODO Auto-generated method stub
		for(int index = 0; index < savingsAccounts.length; index++) {
			if(this.savingsAccounts[index]!= null && this.savingsAccounts[index].getAccountNumber() == accountNumber) {
				return this.savingsAccounts[index];
			}
		}
		throw new CustomerNotFound("User Not Found");

	}

	
}
