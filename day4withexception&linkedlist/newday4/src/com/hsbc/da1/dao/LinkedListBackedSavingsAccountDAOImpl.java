package com.hsbc.da1.dao;
import com.hsbc.da1.model.*;
import java.util.*;
import com.hsbc.da1.exception.*;

public class LinkedListBackedSavingsAccountDAOImpl implements SavingsAccountDAO {
	
	private static List<SavingsAccount> savingsAccounts= new LinkedList<>();
	
	
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
		this.savingsAccounts.add(savingsAccount);
		return savingsAccount;
	}
	
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
		for(SavingsAccount sa : savingsAccounts) {
			if(sa.getAccountNumber() == accountNumber) {
				sa = savingsAccount;
				break;
			}
		}
		return savingsAccount;
	}
	
	public void deleteSavingsAccount(long accountNumber) {
		for(SavingsAccount sa : savingsAccounts) {
			if(sa.getAccountNumber() == accountNumber) {
				this.savingsAccounts.remove(sa);
				break;
			}
		}
	}
	
	public List<SavingsAccount> fetchSavingsAccount() {
		return savingsAccounts;
	}
	
	public SavingsAccount fetchByIdSavingsAccount(long accountNumber) throws CustomerNotFound{
		for(SavingsAccount sa : savingsAccounts) {
			if(sa != null && sa.getAccountNumber() == accountNumber) {
				return sa;
			}
		}
		throw new CustomerNotFound("Customer Not Found!!");
	}

	
	
}
