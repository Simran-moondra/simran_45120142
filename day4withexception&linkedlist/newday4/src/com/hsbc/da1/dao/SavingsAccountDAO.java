package com.hsbc.da1.dao;
import com.hsbc.da1.model.*;
import com.hsbc.da1.exception.*;
import java.util.*;

public interface SavingsAccountDAO {

	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount);
	
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount);
	
	public void deleteSavingsAccount(long accountNumber);
	
	public List<SavingsAccount> fetchSavingsAccount();
	
	public SavingsAccount fetchByIdSavingsAccount(long accountNumber) throws CustomerNotFound;
}
