package com.hsbc.da1.exception;

public class CustomerNotFound extends Exception {

	public CustomerNotFound(String message) {
		super(message);
	}
	
	public String getMessage() {
		return super.getMessage();
	}
}
