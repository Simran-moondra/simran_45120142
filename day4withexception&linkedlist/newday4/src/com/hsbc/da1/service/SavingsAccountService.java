package com.hsbc.da1.service;
import com.hsbc.da1.model.*;
import java.util.*;
import com.hsbc.da1.exception.*;

public interface SavingsAccountService {
	
	
	public SavingsAccount createSavingsAccount(String customerName, double accountBalance);
	
	public void deleteSavingsAccount(long accountNumber);
	
	public List<SavingsAccount> fetchSavingsAccount();
	
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFound;
	
	public double withdraw(long accountNumber, double amount) throws InsufficientBalanceException, CustomerNotFound;
	
	public double deposit(long accountNumber, double amount) throws CustomerNotFound;
	
	public double checkBalance(long accountNumber) throws CustomerNotFound;
	
	
	
}
