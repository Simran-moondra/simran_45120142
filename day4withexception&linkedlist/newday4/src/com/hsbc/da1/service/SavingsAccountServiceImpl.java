package com.hsbc.da1.service;

import java.util.*;

import com.hsbc.da1.dao.*;
import com.hsbc.da1.exception.*;
import com.hsbc.da1.model.*;

public class SavingsAccountServiceImpl implements SavingsAccountService {
	
	private SavingsAccountDAO dao = new LinkedListBackedSavingsAccountDAOImpl();
	
	public SavingsAccount createSavingsAccount(String customerName, double accountBalance) {
		SavingsAccount savingsAccount = new SavingsAccount(customerName, accountBalance);
		SavingsAccount newAcc = dao.saveSavingsAccount(savingsAccount);
		return newAcc;
	}
	
	public void deleteSavingsAccount(long accountNumber) {
		this.dao.deleteSavingsAccount(accountNumber);
	}
	
	public List<SavingsAccount> fetchSavingsAccount() {
		List<SavingsAccount> savingsAccounts = this.dao.fetchSavingsAccount();
		return savingsAccounts;
	}
	
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFound{
		SavingsAccount savingsAccount = this.dao.fetchByIdSavingsAccount(accountNumber);
		return savingsAccount;
	}
	
	public double withdraw(long accountNumber, double amount) throws InsufficientBalanceException, CustomerNotFound{
		SavingsAccount savingsAccount = this.dao.fetchByIdSavingsAccount(accountNumber);
		if((savingsAccount.getAccountBalance() - 10000) > amount) {
			savingsAccount.setAccountBalance(savingsAccount.getAccountBalance() - amount);
			this.dao.updateSavingsAccount(accountNumber, savingsAccount);
			return amount;
		}else {
			throw new InsufficientBalanceException("You do not have enough balance!!");
		}
	}
	
	public double deposit(long accountNumber, double amount) throws CustomerNotFound{
		SavingsAccount savingsAccount = this.dao.fetchByIdSavingsAccount(accountNumber);
		if(savingsAccount != null) {
			savingsAccount.setAccountBalance(savingsAccount.getAccountBalance() + amount);
			this.dao.updateSavingsAccount(accountNumber, savingsAccount);
			return savingsAccount.getAccountBalance();
		}
		return 0;
	}
	
	public double checkBalance(long accountNumber) throws CustomerNotFound{
		SavingsAccount savingsAccount = this.dao.fetchByIdSavingsAccount(accountNumber);
		return savingsAccount.getAccountBalance();
	}

}
