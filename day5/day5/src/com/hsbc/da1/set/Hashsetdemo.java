package com.hsbc.da1.set;

import java.util.*;

public class Hashsetdemo {
	public static void main(String args[]) {
		Set<Integer> set1 = new HashSet<>();
		
		Set<String> set2 = new HashSet<>();
		
		set1.add(20);
		set1.add(50);
		
		set2.add("Hello!");
		set2.add("How are you?");
		
		System.out.printf("Total number of elements in Set1 : %d %n ",set1.size());
		
		System.out.printf("The elements of Set2 : %s",set2.toString());
		
		Iterator<Integer> it = set1.iterator();
		
		while(it.hasNext()) {
			int val = it.next();
			System.out.println("\n the elements are: " +val);
		}
		
	}
}
