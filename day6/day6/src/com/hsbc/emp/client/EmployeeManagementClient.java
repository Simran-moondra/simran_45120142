package com.hsbc.emp.client;
import java.util.*;
import com.hsbc.emp.model.*;
import com.hsbc.emp.exception.*;
import com.hsbc.emp.controller.*;

public class EmployeeManagementClient {
	public static void main(String[] args) {
		
		EmployeeManagementController controller = new EmployeeManagementController();
		
		Employee simran = controller.createEmployeeDetails("Simran", 54000.00, 24);
		Employee shalaka = controller.createEmployeeDetails("Shalaka", 60000.75, 23);
		
		try {
			System.out.println("Simran's leave application for 2 days, hence now Total Leaves left are : "+ controller.leaveApply(simran.getEmployeeId(), 2));
			System.out.println("Simran's leave application for 2 days, hence now Total Leaves left are : "+ controller.leaveApply(simran.getEmployeeId(), 2));
		}catch (EmployeeNotFound | InsufficientforLeaveException e) {
			System.out.println(e.getMessage());
		}
	}
}
