package com.hsbc.emp.controller;
import java.util.*;
import com.hsbc.emp.model.*;
import com.hsbc.emp.dao.*;
import com.hsbc.emp.service.*;
import com.hsbc.emp.exception.*;

public class EmployeeManagementController {
	private EmployeeManagementService empservice = new EmployeeManagementServiceImpl();
	
	public Employee createEmployeeDetails(String employeeName, double employeeSalary, int employeeAge) {
		// TODO Auto-generated method stub
		return this.empservice.createEmployeeDetails(employeeName, employeeSalary, employeeAge);
	}

	public void deleteEmployeeDetails(long employeeId) {
		// TODO Auto-generated method stub
		this.empservice.deleteEmployeeDetails(employeeId);
	}

	public Employee updateEmployeeDetails(long employeeId, Employee employee) {
		// TODO Auto-generated method stub
		return this.empservice.updateEmployeeDetails(employeeId, employee);
	}

	public List<Employee> fetchAllEmployees() {
		// TODO Auto-generated method stub
		return empservice.fetchAllEmployees();
	}

	public Employee fetchEmployeebyId(long employeeId) throws EmployeeNotFound {
		// TODO Auto-generated method stub
	return this.empservice.fetchEmployeebyId(employeeId);
	}

	public List<Employee> fetchEmployeebyName(String employeeName) {
		// TODO Auto-generated method stub
		return empservice.fetchEmployeebyName(employeeName);
	}

	public int leaveApply(long employeeId, int days) throws EmployeeNotFound, InsufficientforLeaveException {
		// TODO Auto-generated method stub
		return this.empservice.leaveApply(employeeId, days);
	}

}
