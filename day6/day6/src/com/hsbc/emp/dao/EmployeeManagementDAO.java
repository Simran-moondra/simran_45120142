package com.hsbc.emp.dao;
import com.hsbc.emp.model.*;
import com.hsbc.emp.exception.*;
import java.util.*;


public interface EmployeeManagementDAO {
	
	Employee saveEmployeeDetails(Employee employee);
	
	Employee updateEmployeeDetails(long employeeId, Employee employee);
	
	public void deleteEmployeeDetails(long employeeId);
	
	List<Employee> fetchAllEmployees();
	
	Employee fetchEmployeebyId(long employeeId) throws EmployeeNotFound;
	
	List<Employee> fetchEmployeebyName(String employeeName);
}
