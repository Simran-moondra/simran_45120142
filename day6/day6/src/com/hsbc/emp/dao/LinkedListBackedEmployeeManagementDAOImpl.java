package com.hsbc.emp.dao;
import java.util.*;
import com.hsbc.emp.model.*;
import com.hsbc.emp.exception.*;

public class LinkedListBackedEmployeeManagementDAOImpl implements EmployeeManagementDAO{
	
	private List<Employee> emps = new LinkedList<>();

	@Override
	public Employee saveEmployeeDetails(Employee employee) {
		// TODO Auto-generated method stub
		this.emps.add(employee);
		return employee;
	}

	@Override
	public Employee updateEmployeeDetails(long employeeId, Employee employee) {
		// TODO Auto-generated method stub
		for(Employee e : emps) {
			if(e.getEmployeeId() == employeeId) {
				e = employee;
			}
		}return employee;
	}

	@Override
	public void deleteEmployeeDetails(long employeeId) {
		// TODO Auto-generated method stub
		for(Employee e : emps) {
			if(e.getEmployeeId() == employeeId) {
				this.emps.remove(e);
			}
		}
	}

	@Override
	public List<Employee> fetchAllEmployees() {
		// TODO Auto-generated method stub
		return emps;
	}

	@Override
	public Employee fetchEmployeebyId(long employeeId) throws EmployeeNotFound {
		// TODO Auto-generated method stub
		for(Employee e : emps) {
			if(e.getEmployeeId() == employeeId) {
				return e;
			}
		}throw new EmployeeNotFound("Employee not found in the Records!!");
	}

	@Override
	public List<Employee> fetchEmployeebyName(String employeeName) {
		// TODO Auto-generated method stub
		List<Employee> emplist = new LinkedList<>();
		for(Employee e : emps) {
			if(e.getEmployeeName() == employeeName) {
				emplist.add(e);
			}
		}return emplist;
	}
	
	

}
