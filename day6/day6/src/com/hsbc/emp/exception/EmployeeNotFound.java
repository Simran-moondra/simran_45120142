package com.hsbc.emp.exception;

public class EmployeeNotFound extends Exception{
	
	public EmployeeNotFound(String message) {
		super(message);
	}
	
	public String getMessage() {
		return super.getMessage();
	}

}

