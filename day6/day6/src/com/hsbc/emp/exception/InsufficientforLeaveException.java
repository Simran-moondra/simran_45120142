package com.hsbc.emp.exception;

public class InsufficientforLeaveException extends Exception{
	
	public InsufficientforLeaveException(String message) {
		super(message);
	}
	
	public String getMessage() {
		return super.getMessage();
	}

}

