package com.hsbc.emp.model;


public class Employee implements Comparable<Employee> {

	private String employeeName;
	
	private long employeeId;
	
	private double employeeSalary;
	
	private int employeeAge;
	
	private int totalLeaves;
	
	private static long empidcounter = 1000;
	
	public Employee(String employeeName, double employeeSalary, int employeeAge) {
		this.employeeName = employeeName;
		this.employeeSalary = employeeSalary;
		this.employeeAge = employeeAge;
		this.employeeId = ++empidcounter;
		this.totalLeaves = 40;
	}
	
	public int getTotalLeaves() {
		return totalLeaves;
	}


	public void setTotalLeaves(int totalLeaves) {
		this.totalLeaves = totalLeaves;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public long getEmployeeId() {
		return employeeId;
	}

	public double getEmployeeSalary() {
		return employeeSalary;
	}

	public int getEmployeeAge() {
		return employeeAge;
	}

	@Override
	public String toString() {
		return "Employee [employeeName=" + employeeName + ", employeeId=" + employeeId + ", employeeSalary="
				+ employeeSalary + ", employeeAge=" + employeeAge + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (employeeId ^ (employeeId >>> 32));
		result = prime * result + ((employeeName == null) ? 0 : employeeName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (employeeId != other.employeeId)
			return false;
		if (employeeName == null) {
			if (other.employeeName != null)
				return false;
		} else if (!employeeName.equals(other.employeeName))
			return false;
		return true;
	}

	@Override
	public int compareTo(Employee emp) {
		// TODO Auto-generated method stub
		return -1 * emp.employeeName.compareTo(this.employeeName);
	}
	
}
