package com.hsbc.emp.service;
import java.util.*;
import com.hsbc.emp.model.*;
import com.hsbc.emp.dao.*;
import com.hsbc.emp.exception.*;

public interface EmployeeManagementService {
	
	public Employee createEmployeeDetails(String employeeName, double employeeSalary, int employeeAge);
	
	public void deleteEmployeeDetails(long employeeId);
	
	public Employee updateEmployeeDetails(long employeeId, Employee employee);
	
	public List<Employee> fetchAllEmployees();
	
	public Employee fetchEmployeebyId(long employeeId) throws EmployeeNotFound;
	
	public List<Employee> fetchEmployeebyName(String employeeName);
	
	public int leaveApply(long employeeId, int days) throws EmployeeNotFound, InsufficientforLeaveException; 
}
