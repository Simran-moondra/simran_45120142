package com.hsbc.emp.service;
import java.util.*;
import com.hsbc.emp.model.*;
import com.hsbc.emp.dao.*;
import com.hsbc.emp.exception.*;

public class EmployeeManagementServiceImpl implements EmployeeManagementService {
	
	private EmployeeManagementDAO dao = new LinkedListBackedEmployeeManagementDAOImpl();

	@Override
	public Employee createEmployeeDetails(String employeeName, double employeeSalary, int employeeAge) {
		// TODO Auto-generated method stub
		Employee employee = new Employee(employeeName,employeeSalary,employeeAge);
		return this.dao.saveEmployeeDetails(employee);
	}

	@Override
	public void deleteEmployeeDetails(long employeeId) {
		// TODO Auto-generated method stub
		this.dao.deleteEmployeeDetails(employeeId);
	}

	@Override
	public Employee updateEmployeeDetails(long employeeId, Employee employee) {
		// TODO Auto-generated method stub
		return this.dao.updateEmployeeDetails(employeeId, employee);
	}

	@Override
	public List<Employee> fetchAllEmployees() {
		// TODO Auto-generated method stub
		return this.dao.fetchAllEmployees();
	}

	@Override
	public Employee fetchEmployeebyId(long employeeId) throws EmployeeNotFound {
		// TODO Auto-generated method stub
		return this.dao.fetchEmployeebyId(employeeId);
	}

	@Override
	public List<Employee> fetchEmployeebyName(String employeeName) {
		// TODO Auto-generated method stub
		return this.dao.fetchEmployeebyName(employeeName);
	}

	@Override
	public int leaveApply(long employeeId, int days) throws EmployeeNotFound, InsufficientforLeaveException {
		// TODO Auto-generated method stub
		Employee employee = this.dao.fetchEmployeebyId(employeeId);
		int leavesleft = employee.getTotalLeaves();
		
		if(employee != null && leavesleft >= days && days <= 10) {
			employee.setTotalLeaves(leavesleft - days);
			updateEmployeeDetails(employeeId, employee);
			return employee.getTotalLeaves();
		}else {
			throw new InsufficientforLeaveException("Sorry! you don't have enough leaves balance left.");
		}
	}

}
