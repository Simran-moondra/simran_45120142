package com.hsbc.da1.io;
import java.io.*;
/*import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;*/

public class FileReadWriteDemo {
	
	public static void main(String[] args) {
		File textFile = new File("G:\\helo.txt");
		
		readwriteFile(textFile, "G:\\welcome.txt");
	}
	
	public static void readwriteFile(File textFile, String destFile) {
		
		try (
				BufferedReader buf = new BufferedReader(new FileReader(textFile));
				BufferedWriter bufw = new BufferedWriter(new FileWriter(new File(destFile)));
				)
		{
			boolean endOfFile = false ;
			while (!endOfFile) {
				String line = buf.readLine();
				if(line != null) {
					bufw.append(line);
					bufw.newLine();
				}else {
					endOfFile = true;
				}
			}
		}
		
		catch(FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}
	}

}
