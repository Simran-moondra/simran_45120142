package com.hsbc.da1.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileReaderDemo {
	
	public static void main(String args[]) {
		
		FileReader fileread = null;
		
		try {
			File textFile = new File("G:\\helo.txt");
			
			fileread = new FileReader(textFile);
			
			boolean endofFile = false;
			while(! endofFile) {
				int characters = fileread.read();
				if(characters != -1) {
					char ch = (char)characters;
					System.out.print(ch);
				}else {
					endofFile = true;
				}
			}
		}
		
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			 if(fileread != null) {
				 try {
					 fileread.close();
				 }catch (IOException e) {
					 e.printStackTrace();
				 }
			 }
		}
		
	}

}
