package com.hsbc.da1.photos;



public class PhotoClient {

public static void main(String[] args) {
		
		Runnable googlephotosfolder = new GooglePhotos();
		Runnable flickrfolder = new Flickr();
		Runnable picassafolder = new Picassa();
		
		Thread googlepics = new Thread (googlephotosfolder);
		googlepics.setName("Google Photos");
		
		
		Thread flickrpics = new Thread (flickrfolder);
		flickrpics.setName("FLICKR");
		
		
		Thread picassapics = new Thread (picassafolder);
		picassapics.setName("PICASSA");

		
		googlepics.start();
		flickrpics.start();
		picassapics.start();
		
		try {
			googlepics.join();
			flickrpics.join();
			picassapics.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("All Social Platform images are displayed.");
		
	}

}


class GooglePhotos implements Runnable {

	@Override
	public void run() {
		System.out.println("************************* GooglePhotoThread "+ Thread.currentThread()+ " start *************************");
		
		for(int i = 0 ;i < 5; i ++) {
			System.out.println("Thread running "  + i + " name: " + Thread.currentThread());
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("************************* GooglePhotoThread "+ Thread.currentThread()+ " end *************************");
	}
}

class Flickr implements Runnable {

	@Override
	public void run() {
		System.out.println("*************************FlickrThread "+ Thread.currentThread()+ " start *************************");
		
		for(int i = 0 ;i < 5; i ++) {
			System.out.println("Thread running "  + i + " name: " + Thread.currentThread());
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("************************* FlickrThread "+ Thread.currentThread()+ " end *************************");
	}
}

class Picassa implements Runnable {

	@Override
	public void run() {
		System.out.println("************************* PicassaThread "+ Thread.currentThread()+ " start *************************");
		
		for(int i = 0 ;i < 5; i ++) {
			System.out.println("Thread running "  + i + " name: " + Thread.currentThread());
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("************************* PicassaThread "+ Thread.currentThread()+ " end *************************");
	}
}

