package com.hsbc.da1.jdbc;

import java.sql.*;

public class JdbcDemo {

	private static final String INSERT_QUERY = "insert into items (name,price)" 
			+ "values ('Samsung_galaxy',25000), ('Lenovo_G50',58000)";
	
	private static final String SELECT_QUERY = "select * from items";
	
	private static String connectString = "jdbc:derby://localhost:1527/mydb";
	private static String username = "admin";
	private static String password = "password";
	
	public static void main(String[] args) {
		
		try(Connection connection = DriverManager.getConnection(connectString,username,password);
				Statement statement = connection.createStatement();) {
			//int updateRows = statement.executeUpdate(INSERT_QUERY);
			//System.out.println("Number of Rows updated : " + updateRows);
			
			ResultSet resultSet  = statement.executeQuery(SELECT_QUERY);
			
			while(resultSet.next()) {
				System.out.println("Item Name : " +resultSet.getString(1) + "\t Price : "+ resultSet.getString(2));
			}
			
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
